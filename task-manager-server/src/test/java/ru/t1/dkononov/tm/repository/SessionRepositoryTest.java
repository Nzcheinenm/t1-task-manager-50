package ru.t1.dkononov.tm.repository;

import liquibase.Liquibase;
import liquibase.exception.LiquibaseException;
import org.jetbrains.annotations.NotNull;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.t1.dkononov.tm.api.repository.dto.ISessionDTORepository;
import ru.t1.dkononov.tm.api.repository.dto.IUserOwnedDTORepository;
import ru.t1.dkononov.tm.api.services.IConnectionService;
import ru.t1.dkononov.tm.dto.model.SessionDTO;
import ru.t1.dkononov.tm.exception.entity.ProjectNotFoundException;
import ru.t1.dkononov.tm.exception.field.UserIdEmptyException;
import ru.t1.dkononov.tm.marker.DataCategory;
import ru.t1.dkononov.tm.migration.AbstractSchemaTest;
import ru.t1.dkononov.tm.repository.dto.SessionDTORepository;
import ru.t1.dkononov.tm.repository.dto.UserDTORepository;
import ru.t1.dkononov.tm.repository.model.SessionRepository;
import ru.t1.dkononov.tm.service.ConnectionService;
import ru.t1.dkononov.tm.service.PropertyService;

import javax.persistence.EntityManager;

import static ru.t1.dkononov.tm.constant.TestData.SESSION;
import static ru.t1.dkononov.tm.constant.TestData.USER1;

@Category(DataCategory.class)
public class SessionRepositoryTest extends AbstractSchemaTest {

    @NotNull
    private final IConnectionService connectionService = new ConnectionService(new PropertyService());

    @NotNull
    private final EntityManager entityManager = connectionService.getEntityManager();
    @NotNull
    private final IUserOwnedDTORepository<SessionDTO> repository = new SessionDTORepository(entityManager);

    @BeforeClass
    public void setup() throws LiquibaseException {
        final Liquibase liquibase = liquibase("changelog/changelog-master.xml");
        liquibase.dropAll();
        liquibase.update("scheme");

        PropertyService propertyService = new PropertyService();
        ConnectionService connectionService = new ConnectionService(propertyService);
    }

    @Before
    public void init() {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final SessionDTORepository repository = new SessionDTORepository(entityManager);
            @NotNull final UserDTORepository userDTORepository = new UserDTORepository(entityManager);
            entityManager.getTransaction().begin();
            userDTORepository.add(USER1);
            repository.add(USER1.getId(), SESSION);
            ;
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @After
    public void after() {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ISessionDTORepository repository = new SessionDTORepository(entityManager);
            entityManager.getTransaction().begin();
            repository.clear(USER1.getId());
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Test
    public void addByUserId() throws UserIdEmptyException, ProjectNotFoundException {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final SessionDTORepository repository = new SessionDTORepository(entityManager);
            repository.add(USER1.getId(), SESSION);
            Assert.assertNotNull(repository.findById(SESSION.getId()));
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Test
    public void createByUserId() {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final SessionDTORepository repository = new SessionDTORepository(entityManager);
            Assert.assertEquals(SESSION.getUserId(), USER1.getId());
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Test
    public void findAllNull() {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final SessionDTORepository repository = new SessionDTORepository(entityManager);
            @NotNull final SessionRepository sessionRepository = new SessionRepository(connectionService.getEntityManager());
            Assert.assertTrue(sessionRepository.findAll().isEmpty());
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Test
    public void findByNullId() {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final SessionDTORepository repository = new SessionDTORepository(entityManager);
            Assert.assertNull(repository.findById(USER1.getId(), null));
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

}
